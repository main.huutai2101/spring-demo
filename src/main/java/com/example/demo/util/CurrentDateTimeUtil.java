package com.example.demo.util;

import lombok.experimental.UtilityClass;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.Date;

@UtilityClass
public class CurrentDateTimeUtil {

    private static Clock CLOCK;

    public static void of(Clock clock) {
        CLOCK = clock;
    }

    public static ZonedDateTime currentZonedDateTime() {
        return ZonedDateTime.now(CLOCK);
    }

    public static LocalDateTime currentLocalDateTime() {
        return currentZonedDateTime().toLocalDateTime();
    }

    public static LocalDate currentLocalDate() {
        return currentLocalDateTime().toLocalDate();
    }

    public static LocalTime currentLocalTime() {
        return currentLocalDateTime().toLocalTime();
    }

    public static Date currentDate() {
        return Date.from(currentZonedDateTime().toInstant());
    }


}
