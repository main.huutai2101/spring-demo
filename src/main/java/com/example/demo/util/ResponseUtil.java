package com.example.demo.util;

import com.example.demo.dto.pagination.PageableDto;
import lombok.experimental.UtilityClass;

import java.util.List;
import java.util.Map;

@UtilityClass
public class ResponseUtil {

    public <T> Map<String, Object> withPagination(PageableDto pagination, List<T> data) {
        return Map.of("pagination", pagination, "data", data);
    }
}
