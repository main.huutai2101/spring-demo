package com.example.demo.config;

import com.example.demo.util.CurrentDateTimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.annotation.ApplicationScope;

import javax.annotation.PostConstruct;
import java.time.Clock;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Configuration
public class TimeTravelConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(TimeTravelConfig.class);

    @Value("${timetravel.target-date-time:}")
    private String timeTravelTargetDateTime;

    @PostConstruct
    public void init() {
        Clock travelClock = null;
        try {
            ZonedDateTime currentZonedDateTime = ZonedDateTime.now();
            ZonedDateTime travelZonedDateTime = LocalDateTime.parse(timeTravelTargetDateTime, DateTimeFormatter.ISO_DATE_TIME).atZone(ZoneId.systemDefault());
            travelClock = Clock.offset(Clock.systemDefaultZone(), Duration.between(currentZonedDateTime, travelZonedDateTime));
            LOGGER.info("Time travel is initialized");
        } catch (Exception e) {
            travelClock = Clock.systemDefaultZone();
            LOGGER.warn("Time travel is not initialized - {}", e.getMessage());
        } finally {
            CurrentDateTimeUtil.of(travelClock);
        }
    }
}
