package com.example.demo.product;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Getter @Setter
@ToString
@Entity
public class Product {

    @Id
    @GeneratedValue
    private UUID uuid;
    private String name;
    private Double price;

}