package com.example.demo.product;

import com.example.demo.dto.pagination.PageableDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getProducts(PageableDto pageableDto) {
        return productRepository.findAll(pageableDto.toPageable()).toList();
    }

    public Product save(Product product) {
        return productRepository.save(product);
    }

    public long count() {
        return productRepository.count();
    }
}
