package com.example.demo.service.greenbeli.property;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter @Setter
@Component
@ConfigurationProperties(prefix = "greenbeli")
public class GreenbeliProperties {

    private Auth auth;

    @Getter @Setter
    public static class Auth {

        private String username;
        private String password;

    }

}

