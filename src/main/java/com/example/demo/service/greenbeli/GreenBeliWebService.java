package com.example.demo.service.greenbeli;

import com.example.demo.service.greenbeli.dto.GreenBeliAccessTokenDto;
import com.example.demo.service.greenbeli.dto.GreenBeliRefreshTokenDto;
import com.example.demo.service.greenbeli.dto.GreenBeliTokenDto;
import com.example.demo.service.greenbeli.dto.GreenBeliUserDto;
import com.example.demo.service.greenbeli.exception.GreenBeliException;
import com.example.demo.service.greenbeli.property.GreenbeliProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

import static com.example.demo.service.greenbeli.constant.GreenBeliUri.BASE_URL;
import static com.example.demo.service.greenbeli.constant.GreenBeliUri.LOGIN;
import static com.example.demo.service.greenbeli.constant.GreenBeliUri.REFRESH;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

@Component
public class GreenBeliWebService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GreenBeliWebService.class);
    private static final RestTemplate REST_TEMPLATE = new RestTemplate();

    private final GreenbeliProperties greenbeliProperties;

    private GreenBeliTokenDto greenBeliTokenDto;

    public GreenBeliWebService(GreenbeliProperties greenbeliProperties) {
        this.greenbeliProperties = greenbeliProperties;
    }

    @PostConstruct
    public void init() {
        this.greenBeliTokenDto = new GreenBeliTokenDto();
    }

    public String getSomething() {
        try {
            ResponseEntity<String> response = REST_TEMPLATE.exchange(BASE_URL, GET, new HttpEntity<>(null, headers()), String.class);
            if (response.getStatusCode().is2xxSuccessful() && response.getBody() != null) {
                return response.getBody();
            } else {
                return null;
            }
        } catch (HttpClientErrorException.Unauthorized unauthorized) {
            updateToken();
            return getSomething();
        }
    }

    private MultiValueMap<String, String> headers() {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Authorization", "Bearer " + this.greenBeliTokenDto.getAccessToken());
        return headers;
    }

    private void login() {
        String user = greenbeliProperties.getAuth().getUsername();
        String pass = greenbeliProperties.getAuth().getPassword();
        GreenBeliUserDto greenBeliUserDto = new GreenBeliUserDto(user, pass);

        ResponseEntity<GreenBeliTokenDto> response = REST_TEMPLATE.exchange(LOGIN, POST, new HttpEntity<>(greenBeliUserDto), GreenBeliTokenDto.class);
        if (response.getStatusCode().is2xxSuccessful() && response.getBody() != null) {
            this.greenBeliTokenDto = response.getBody();
        } else {
            throw new GreenBeliException("'login' with empty response body!");
        }
    }

    private void renewAccessToken() {
        GreenBeliRefreshTokenDto refreshTokenDto = new GreenBeliRefreshTokenDto(this.greenBeliTokenDto.getRefreshToken());
        ResponseEntity<GreenBeliAccessTokenDto> response = REST_TEMPLATE.exchange(REFRESH, POST, new HttpEntity<>(refreshTokenDto), GreenBeliAccessTokenDto.class);
        if (response.getStatusCode().is2xxSuccessful() && response.getBody() != null) {
            this.greenBeliTokenDto.setAccessToken(response.getBody().getAccessToken());
        } else {
            throw new GreenBeliException("'renewAccessToken' with empty response body!");
        }
    }

    private void updateToken() {
        try {
            renewAccessToken();
        } catch (HttpClientErrorException.Unauthorized unauthorized) {
            LOGGER.warn("Cannot renew access token!", unauthorized);
            login();
        }
    }

}
