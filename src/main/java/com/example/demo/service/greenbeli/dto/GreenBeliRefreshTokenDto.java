package com.example.demo.service.greenbeli.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GreenBeliRefreshTokenDto {

    @JsonProperty("refresh_token")
    private String refreshToken = "";

}
