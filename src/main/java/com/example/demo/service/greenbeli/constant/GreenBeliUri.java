package com.example.demo.service.greenbeli.constant;

import lombok.experimental.UtilityClass;

@UtilityClass
public class GreenBeliUri {
    public static final String BASE_URL = "https://api.greenbeli.io/common/heroes";
    public static final String LOGIN = BASE_URL + "/login";
    public static final String REFRESH = BASE_URL + "/refresh";
}
