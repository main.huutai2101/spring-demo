package com.example.demo.service.greenbeli.exception;

public class GreenBeliException extends RuntimeException {

    public GreenBeliException(String message) {
        super(message);
    }

    public GreenBeliException(String message, Throwable cause) {
        super(message, cause);
    }

    public GreenBeliException(Throwable cause) {
        super(cause);
    }
}
