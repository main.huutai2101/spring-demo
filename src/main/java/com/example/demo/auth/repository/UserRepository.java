package com.example.demo.auth.repository;

import com.example.demo.auth.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
	Boolean existsByUsername(String username);
	Boolean existsByEmail(String email);
}