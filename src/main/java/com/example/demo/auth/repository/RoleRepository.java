package com.example.demo.auth.repository;

import com.example.demo.auth.model.ERole;
import com.example.demo.auth.model.Role;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends PagingAndSortingRepository<Role, Long> {
	Optional<Role> findByName(ERole name);
}