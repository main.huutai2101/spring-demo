package com.example.demo.dto.pagination;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import static org.springframework.data.domain.Sort.Direction.*;

@Getter @Setter
@ToString
public class PageableDto {

    private int pageNumber = 0;
    private int pageSize = 10;
    private long totalItems = 0L;

    @JsonIgnore
    private String sortBy = "uuid";
    @JsonIgnore
    private String sortOrder = "asc";

    public Pageable toPageable() {
        return PageRequest.of(pageNumber, pageSize, fromString(sortOrder), sortBy);
    }
}
